import java.util.Scanner;
public class Desafio1{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Insira o valor que deseja sacar: ");
        int v = input.nextInt();
        int v100 = v/100;
        v = v%100;
        int v50 = v/50;
        v = v%50;
        int v20 = v/20;
        v = v%20;
        int v10 = v/10;
        v = v%10;
        int v5 = v/5;
        v = v%5;
        int v2 = v/2;
        v = v%2;
        System.out.println("Total de cédulas de 100: " + v100);
        System.out.println("Total de cédulas de 50: " + v50);
        System.out.println("Total de cédulas de 20: " + v20);
        System.out.println("Total de cédulas de 10: " + v10);
        System.out.println("Total de cédulas de 5: " + v5);
        System.out.println("Total de cédulas de 2: " + v2);
        System.out.println("Total de cédulas de 1: " + v);



    }

}